from psycogreen.gevent import patch_psycopg
import os

bind = '0.0.0.0:8000'

reload = True
workers = 4
worker_class = 'gevent'
worker_connections = 1000
timeout = 7200

accesslog = os.environ.get('log_root') + '/its_gunicorn.log'
errorlog = os.environ.get('log_root') + '/its_gunicorn_error.log'

loglevel = 'debug'

proc_name = 'inventory'

def post_fork(server, worker):
    # patch psycopg2 for gevent compatibility
    patch_psycopg()
