# -*- coding: utf-8 -*-
"""Create an application instance."""
from flask_poc.app import create_app
from flask_poc.config.dev_config import DevConfig as dev
from flask_poc.config.local_config import LocalConfig as local
from flask_poc.config.prod_config import ProdConfig as prod
from flask_poc.config.staging_config import StagingConfig as staging
from flask_poc.config.test_config import TestConfig as test
from flask_poc.settings import FlaskEnvironment


class MainApp(object):
    flask_env = FlaskEnvironment()

    @staticmethod
    def get_app():
        config = local
        if str(MainApp.flask_env.env).lower() == 'prod':
            config = prod
        elif str(MainApp.flask_env.env).lower() == 'staging':
            config = staging
        elif str(MainApp.flask_env.env).lower() == 'dev':
            config = dev
        elif str(MainApp.flask_env.env).lower() == 'test':
            config = test
        return create_app(config)


app = MainApp().get_app()
