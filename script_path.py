gunicorn_script = '/usr/src/app/gunicorn_start'
worker_scripts = {
    'flask_poc_consumer': '/usr/src/app/flask_poc_consumer_start'

}

celery_script = '/usr/src/app/celery_script'
celery_beat_script = '/usr/src/app/celery_beat_script'

cron_script = '/bin/sh -c "crontab cron_jobs; crond -f"'
