#!/bin/bash

NAME="rcs_consumer_start"
echo "Starting $NAME as `whoami`"

SOCKFILEPREFIX=/var/run/gunicorn.sock
SOCKFILE=$SOCKFILEPREFIX$1  # we will communicte using this unix socket

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
echo $RUNDIR
test -d $RUNDIR || mkdir -p $RUNDIR

PROJECT_BASE=/usr/src/app/promo_utility
export PYTHONPATH=$PROJECT_BASE:$PYTHONPATH

echo "Starting Flask POC Listener"
python manage.py rcs_consumer
