# -*- coding: utf-8 -*-
"""The app module, containing the app factory function."""
import flask_admin
from flask import Flask, render_template
import flask_poc.api.flask_poc_apis as flask_poc_apis
from flask_poc import admin_views
from flask_poc import commands as cmds, public, user
from flask_poc.extensions import bcrypt, db, debug_toolbar, login_manager, migrate, cache
from flask_poc.extensions import ma
from flask_poc.admin_views.index import MyAdminIndexView
from flask_poc.producers.thread_pool import init_threadpool


def create_app(config):
    """An application factory, as explained here: http://flask.pocoo.org/docs/patterns/appfactories/.

    :param config: The configuration object to use.
    """

    app = Flask(__name__.split('.')[0])
    app.config.from_object(config)
    register_extensions(app)
    register_blueprints(app)
    register_errorhandlers(app)
    register_shellcontext(app)
    register_commands(app)
    configure_logging(app)
    initialize_threadpool(app)
    admin = flask_admin.Admin(app, index_view=MyAdminIndexView(url='/flask_poc/admin'), base_template='my_master.html')

    setup_admin(app, admin)
    return app


def setup_admin(app, admin):
    for admin_view in admin_views.__all__:
        model_view = getattr(admin_views, admin_view)
        admin.add_view(model_view(model_view._model, db.session))


def register_extensions(app):
    """Register Flask extensions."""
    bcrypt.init_app(app)
    cache.init_app(app, config={'CACHE_TYPE': 'simple'})
    db.init_app(app)
    ma.init_app(app)
    login_manager.init_app(app)
    debug_toolbar.init_app(app)
    migrate.init_app(app, db)
    return None


def register_blueprints(app):
    """Register Flask blueprints."""
    app.register_blueprint(public.views.blueprint)
    app.register_blueprint(user.views.blueprint)
    app.register_blueprint(flask_poc_apis.blueprint)
    return None


def register_errorhandlers(app):
    """Register error handlers."""

    def render_error(error):
        """Render error template."""
        # If a HTTPException, pull the `code` attribute; default to 500
        error_code = getattr(error, 'code', 500)
        return render_template('{0}.html'.format(error_code)), error_code

    for errcode in [401, 404, 500]:
        app.errorhandler(errcode)(render_error)
    return None


def register_shellcontext(app):
    """Register shell context objects."""

    def shell_context():
        """Shell context objects."""
        return {
            'db': db,
            'User': user.models.User
        }

    app.shell_context_processor(shell_context)


def register_commands(app):
    """Register Click commands."""
    app.cli.add_command(cmds.test)
    app.cli.add_command(cmds.lint)
    app.cli.add_command(cmds.clean)
    app.cli.add_command(cmds.urls)
    app.cli.add_command(cmds.event_consumer)
    app.cli.add_command(cmds.local_test_consumer)


def initialize_threadpool(app):
    init_threadpool(app)


def configure_logging(app):
    import logging.config

    logger_setting = {
        'version': 1,
        'filters': {
            'request_id': {
                '()': 'flask_poc.utils.RequestIdFilter',
            },
        },
        'disable_existing_loggers': False,
        'formatters': {
            'verbose': {
                'format': "[%(asctime)s] %(levelname)s %(request_id)s [%(name)s:%(lineno)s] %(message)s",
                'datefmt': "%Y-%m-%d %H:%M:%S"
            },
            'logstash_fmtr': {
                '()': 'logstash_formatter.LogstashFormatterV1',
            },
        },
        'handlers': {
            'null': {
                'level': 'INFO',
                'class': 'logging.NullHandler',
                'filters': ['request_id'],
            },
            'console': {
                'level': 'INFO',
                'class': 'logging.StreamHandler',
                'formatter': 'verbose',
                'filters': ['request_id'],
            },
            'its': {
                'level': 'DEBUG',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': app.config['LOG_ROOT'] + '/flask_poc.log',
                'formatter': 'logstash_fmtr',
                'filters': ['request_id'],
            },
        },
        'loggers': {
            'flask_poc': {
                'handlers': ['its', 'console'],
                'level': 'DEBUG',
                'propagate': True,
            },
        },
    }

    logging.config.dictConfig(logger_setting)
