import concurrent.futures

executor = None


def init_threadpool(app):
    global executor
    executor = concurrent.futures.ThreadPoolExecutor(max_workers=int(app.config.get('EVENT_PRODUCER_THREAD_COUNT')))
