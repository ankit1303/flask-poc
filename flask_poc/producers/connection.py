from kombu import BrokerConnection
import logging

logger = logging.getLogger(__name__)


class Connection:
    class __Connection:
        def __init__(self, arg):
            logger.info("Starting connection to - %s", arg)
            self.conn = BrokerConnection(arg)
            logger.info("Connection Establised - OK")

        def __str__(self):
            return repr(self) + self.conn

    instance = None

    def __init__(self, arg):
        if not Connection.instance:
            Connection.instance = Connection.__Connection(arg)

    def __getattr__(self, name):
        return getattr(self.instance, name)
