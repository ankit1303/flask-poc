import logging
from flask import current_app
from kombu.entity import Exchange
from .base_producer import BaseProducer
from flask_poc.consumers.event_type import EventType

logger = logging.getLogger(__name__)


class FlaskPocValueProducer(BaseProducer):
    def __init__(self, event_obj):
        self.exchange_name = str(current_app.config.get('FLASK_POC_VALUE_EXCHANGE'))
        self.event_obj = event_obj
        self.exchange = Exchange(self.exchange_name, type='direct')
        self.routing_key = str(current_app.config.get('FLASK_POC_ROUTING_KEY')) + str(EventType.FlaskPocEvent)
        super(FlaskPocValueProducer, self).__init__(self.exchange, self.exchange_name, self.routing_key, self.event_obj)
