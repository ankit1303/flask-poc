import logging

from flask import current_app
from kombu.common import maybe_declare
from kombu.pools import producers

from flask_poc.producers import thread_pool
from .connection import Connection

logger = logging.getLogger(__name__)


class EventException(Exception):
    pass


class BaseProducer(object):
    def __init__(self, exchange, exchange_name, routing_key, event_obj):
        self.broker_url = str(current_app.config.get('RABBITMQ_BROKER_URL'))
        self.connection = Connection(self.broker_url).__getattr__('conn')
        self.exchange = exchange
        self.exchange_name = exchange_name
        self.routing_key = routing_key
        self.event_obj = event_obj

    def publish_event(self):
        thread_pool.executor.submit(self.send_event)

    def send_event(self):
        logger.info("executing in the thread started")
        connection = self.connection
        with producers[connection].acquire(block=True) as producer:
            try:
                maybe_declare(self.exchange, producer.channel)

                payload = self.event_obj
                logger.info(
                    "Publishing event to - Routing key is : %s , exchange name is : %s , payload : %s",
                    self.routing_key, self.exchange_name, payload)
                producer.publish(
                    payload,
                    exchange=self.exchange_name,
                    serializer="json",
                    routing_key=self.routing_key)
            except Exception as ex:
                logger.exception("Error while publishing event")
                raise EventException(ex)

    @staticmethod
    def fullname(o):
        _module = o.__class__.__module__
        if _module is None or _module == str.__class__.__module__:
            return o.__class__.__name__
        return _module + '.' + o.__class__.__name__
