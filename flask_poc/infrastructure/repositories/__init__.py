from flask_poc.infrastructure.repositories.data_repository import DataRepository


class RepoProvider(object):
    def __init__(self):
        self.data_repository = DataRepository()


repo_provider = RepoProvider()
