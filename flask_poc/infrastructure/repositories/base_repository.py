from flask_poc import extensions


class BaseRepository(object):
    def __init__(self, db=None):
        self.db = db or extensions.db

    @property
    def session(self):
        return self.db.session

    def create(self, item):
        self.session().add(item)
        self.session().commit()
        return item

    def expunge(self, items):
        """
        Use this method if you want a bunch of objects produced by querying a session
        to be usable outside the scope of the session
        Not using this will result in error:
            sqlalchemy.orm.exc.DetachedInstanceError: Instance is not bound to a Session; attribute refresh operation cannot proceed # noqa
        http://stackoverflow.com/questions/8253978/sqlalchemy-get-object-not-bound-to-a-session
        """
        for item in items:
            self.session().expunge(item)

    def commit(self):
        self.session().commit()

    def save(self, entity):
        self.session().add(entity)
        self.session().commit()

    def save_or_update_all(self, entities):
        """
        http://docs.sqlalchemy.org/en/latest/_modules/examples/performance/bulk_inserts.html

        We can use 2 methods for bulk insert in SQL Alchemy:
            - add_all() [http://docs.sqlalchemy.org/en/latest/orm/session_api.html#sqlalchemy.orm.session.Session.add_all] and
            - bulk_save_objects() [http://docs.sqlalchemy.org/en/latest/orm/session_api.html#sqlalchemy.orm.session.Session.bulk_save_objects]

        Using add_all() here. The second method is supposed to be faster, but doesn't populate primary key,
        unless explicitly asked, in which case, the performance gain is lost. We can benchmark that, and switchover.

        :param throttled_availabilities:
        :return:
        """
        self.session().add_all(entities)
        self.session().commit()
