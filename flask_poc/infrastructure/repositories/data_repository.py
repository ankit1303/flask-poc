from flask_poc.infrastructure.repositories.base_repository import BaseRepository
from flask_poc.models.data import Data
import datetime


class DataRepository(BaseRepository):
    def __init__(self):
        self.__model = Data
        BaseRepository.__init__(self)

    def query(self):
        return self.session().query(self.__model)

    def get_all_data(self):
        return self.session().query(Data).all()

    def get_data_for_a_date(self, date,):
        return self.session().query(Data).filter(Data.date == date).first()

    def get_data_for_dates(self, date1,date2):
        return self.session().query(Data).filter(Data.date.between(date1,date2))

    def create_or_update_value_for_a_date(self, data):
        target_data = self.session().query(Data).filter(Data.date == data.date).first()
        if target_data:
            target_data.value = data.value
            self.commit()
        else:
            self.create(data)
