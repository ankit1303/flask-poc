# -*- coding: utf-8 -*-
"""Application configuration."""
import os
from flask_poc.config.local_config import LocalConfig as local
from flask_poc.config.prod_config import ProdConfig as prod
from flask_poc.config.dev_config import DevConfig as dev
from flask_poc.config.test_config import TestConfig as test

class FlaskEnvironment(object):

    env = os.environ.get('ENV', "local")
    __instance = None

    def __init__(self,):
        if FlaskEnvironment.__instance == None:
            FlaskEnvironment.__instance = super(FlaskEnvironment, self).__init__()
