# -*- coding: utf-8 -*-
"""User views."""
import logging
import json

from flask import Blueprint, request

from flask_poc.decorators import json_response
from flask_poc.domain import service_provider
from flask_poc.utils.dateutils import ymd_str_to_date
from flask_poc.extensions import cache

blueprint = Blueprint('flask_poc_blueprint', __name__, url_prefix='/flask_poc', static_folder='../static')
logger = logging.getLogger(__name__)


@blueprint.route('/get_sum_of_values')
@json_response()
def get_sum_of_values():
    logger.info("api call to get_sum_of_values")
    start_date = ymd_str_to_date(request.args.get('start_date', None))
    end_date = ymd_str_to_date(request.args.get('end_date', None))
    sum_of_values = service_provider.data_service.get_sum_of_values_for_a_date_range(start_date, end_date)

    return dict(sum=sum_of_values), 200


@blueprint.route('/update_values', methods=["POST"])
@json_response()
def update_values():
    logger.info("api call tp update_values")
    if request.method == 'POST':
        post_data = request.json
        for data in post_data:
            logger.info("data: " + json.dumps(data))
            service_provider.data_service.publish_data(data)
            cache.clear()

        return {"message": "values updated successfully"}, 200
