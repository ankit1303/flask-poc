from flask_poc.config.base_config import Base


class TestConfig(Base):
    TESTING = True
    DEBUG = True
    LOG_ROOT = "./logs"
    SQLALCHEMY_DATABASE_URI = 'postgresql://poc:poc@localhost:5432/flask_poc'
    BCRYPT_LOG_ROUNDS = 4  # For faster tests; needs at least 4 to avoid "ValueError: Invalid rounds"
    WTF_CSRF_ENABLED = False  # Allows form testing
