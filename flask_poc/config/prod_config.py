from flask_poc.config.base_config import Base


class ProdConfig(Base):
    ENV = 'prod'
    DEBUG = False
    DEBUG_TB_ENABLED = False
