import json
import os


class Base(object):
    SECRET_KEY = os.environ.get('FLASK_POC', 'secret-key')  # TODO: Change me
    APP_DIR = os.path.abspath(os.path.dirname(__file__))  # This directory
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
    BCRYPT_LOG_ROUNDS = 13
    LOG_ROOT = "/var/log/flask_poc"
    DEBUG_TB_ENABLED = False  # Disable Debug toolbar
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    CACHE_TYPE = 'simple'  # Can be "memcached", "redis", etc.
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    WEBPACK_MANIFEST_PATH = 'webpack/manifest.json'
    RABBITMQ_BROKER_URL = os.environ.get('FLASK_POC_RMQ_URL', "localhost")
    FLASK_POC_VALUE_EXCHANGE = "flask_poc_value_exchange"
    FLASK_POC_ROUTING_KEY = 'flask_poc_routing_key'
    FLASK_POC_QUEUE_NAME = 'flask_poc_queue'

    SQLALCHEMY_DATABASE_URI = os.environ.get('FLASK_POC_URL', 'postgresql://localhost/example')
    EVENT_PRODUCER_THREAD_COUNT = 10

    WTF_CSRF_ENABLED = False


class LogstashJsonEncoder(json.JSONEncoder):
    def __init__(self, skipkeys=False, ensure_ascii=True,
                 check_circular=True, allow_nan=True, sort_keys=False,
                 indent=None, separators=None, encoding='utf-8', default=None):
        super(LogstashJsonEncoder, self).__init__(skipkeys=True, ensure_ascii=ensure_ascii,
                                                  check_circular=check_circular,
                                                  allow_nan=allow_nan, sort_keys=sort_keys,
                                                  indent=indent, separators=separators,
                                                  encoding=encoding, default=default)
