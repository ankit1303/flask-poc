from flask_poc.config.base_config import Base


class LocalConfig(Base):
    ENV = 'local'
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://poc:poc@localhost:5432/flask_poc'
    LOG_ROOT = "/var/log/poc"
    WTF_CSRF_ENABLED = False
    DEBUG_TB_ENABLED = True
    CACHE_TYPE = 'simple'
