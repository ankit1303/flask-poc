from flask_poc.config.base_config import Base


class StagingConfig(Base):
    ENV = 'staging'
    DEBUG = True
    CACHE_TYPE = 'simple'  # Can be "memcached", "redis", etc.
