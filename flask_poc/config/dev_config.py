from flask_poc.config.base_config import Base


class DevConfig(Base):
    ENV = 'dev'
    DEBUG = True
    LOG_ROOT = "/var/log/flask_poc"
    DEBUG_TB_ENABLED = True
    CACHE_TYPE = 'simple'
