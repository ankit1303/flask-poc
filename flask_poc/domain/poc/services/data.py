from flask_poc.utils.dateutils import date_range_inclusive
from flask_poc.producers.flask_poc_value_producer import FlaskPocValueProducer
from flask_poc.extensions import cache
import logging

logger = logging.getLogger(__name__)


class DataService(object):
    def __init__(self, data_repository):
        self.data_repository = data_repository

    def get_sum_of_values_for_a_date_range(self, start_date, end_date):
        logger.info("getting sum of values for start_date: %s , end_date: %s", start_date, end_date)
        sum = 0
        dates = self.data_repository.get_data_for_dates(start_date,end_date)
        # for date in date_range_inclusive(start_date, end_date):
        #     try:
        #         value = self.data_repository.get_data_for_a_date(date).value
        #     except:
        #         value = 0
        sum = 0
        for date in dates:
            sum += date.value

        return sum

    def _create_publish_payload(self, data):
        return {"date": data.get('date'), "value": data.get('value')}

    def publish_data(self, data):
        payload = self._create_publish_payload(data)
        FlaskPocValueProducer(payload).publish_event()

    def update_value_for_a_date(self, data):
        logger.info("updating value for date: %s", data.date)
        self.data_repository.create_or_update_value_for_a_date(data)
