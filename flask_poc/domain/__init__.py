from flask_poc.domain.poc.services.data import DataService
from flask_poc.infrastructure.repositories import repo_provider as rp


class ServiceProvider(object):
    def __init__(self):
        self.data_service = DataService(rp.data_repository)


service_provider = ServiceProvider()
