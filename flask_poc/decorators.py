from .extensions import db
import functools
import json
from flask import jsonify, make_response


def close_db_connections():
    """
    Decorator to explicitly close db connections during threaded execution
    """

    def decorator(func):

        @functools.wraps(func)
        def _close_db_connections(*args, **kwargs):
            ret = None
            try:
                ret = func(*args, **kwargs)
            finally:
                db.session.expire_all()
            return ret

        return _close_db_connections

    return decorator


def json_response():
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            data, status_code = func(*args, **kwargs)
            if isinstance(data, dict):
                return make_response(jsonify(data), status_code)
            else:
                return json.dumps(data)

        return wrapper

    return decorator
