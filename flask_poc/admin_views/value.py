import flask_login as login
from flask_admin.contrib.sqla import ModelView
from wtforms import ValidationError

from flask_poc.models.data import Data


class ValueAdminView(ModelView):
    _model = Data
    column_searchable_list = ('date', 'value')
    column_list = ('date', 'value')
    column_filters = ['date', 'value']
    column_display_pk = True
    form_columns = ['date', 'value']
    can_create = True
    can_delete = True
    can_edit = True

    def is_accessible(self):
        return login.current_user.is_authenticated
