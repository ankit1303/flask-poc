import importlib
import logging

from kombu.connection import BrokerConnection
from kombu.mixins import ConsumerMixin

from .decorators import remove_db_session

logger = logging.getLogger(__name__)


class BaseConsumer(ConsumerMixin):
    def __init__(self, broker_url):
        self.connection = BrokerConnection(broker_url)

    def start_consumer(self):
        try:
            self.run()
        except KeyboardInterrupt:
            logger.exception("Exiting the consumer process")
        except Exception:
            logger.exception("Exception occured while consuming")
            self.start_consumer()

    @remove_db_session
    def handle_message(self, body: dict, message):
        logger.info("Message received - %s with type - %s", body, type(body))
        try:
            flag = body.get("__class__")
        except:
            flag = False
        try:
            if flag:
                consumer = str(body['__class__'])
                module_name, class_name = consumer.rsplit(".", 1)
                logger.info("module  %s , class %s ", module_name, class_name)
                _cls = getattr(
                    importlib.import_module(module_name), class_name)
                logger.info("Class resolved: %s", _cls)
                self.on_message(_cls(**body))
            else:
                self.on_message(body)
        except Exception as ex:
            logger.exception("Exception occurred calling on_message")

        message.ack()

    def on_message(self, event_obj):
        logger.error("base method called")
        raise NotImplementedError("Cannot be implemented")

    def get_consumers(self, consumer, channel):
        return [consumer(self.queue, callbacks=[self.handle_message])]
