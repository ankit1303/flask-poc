import logging

from flask_poc.domain import service_provider
from flask_poc.consumers.direct_consumer import DirectConsumer
from flask_poc.consumers.event_type import EventType
from flask_poc.models.data import Data

logger = logging.getLogger(__name__)


class FlaskPocDirectConsumer(DirectConsumer):
    def __init__(self):
        super(FlaskPocDirectConsumer, self).__init__(EventType.FlaskPocEvent)

    def on_message(self, event_obj):
        logger.info("Received FlaskPocEvent: %s", event_obj)
        data = Data()
        data.date = event_obj.get("date")
        data.value = event_obj.get("value")

        service_provider.data_service.update_value_for_a_date(data)
