import importlib
import logging

logger = logging.getLogger(__name__)


class EventException(Exception):
    pass


class EventConsumer(object):
    def handle(self, *args, **options):
        logger.info("Starting Consumer")
        consumer_list = [options['consumer_name']]  # settings.DOMAIN_EVENT_CONSUMER_LIST
        if len(consumer_list) == 0:
            logger.error("No Listeners to start,stopping consumer. Configure list of consumer classes & start again")
            raise EventException(
                "No Listeners to start,stopping consumer. Configure list of consumer classes & start again")
        max_workers = 2 * len(consumer_list)
        logger.info("No. of Listeners configured : %s No. of threads : %s ", len(consumer_list), max_workers)
        logger.info("Starting below listeners")
        counter = 1
        for consumer in consumer_list:
            module_name, class_name = consumer.rsplit(".", 1)
            logger.info("module  %s , class %s ", module_name, class_name)
            _cls = getattr(importlib.import_module(module_name), class_name)
            if _cls:
                _cls().start_consumer()
                logger.info("%s. %s", counter, consumer)
                counter += 1
        logger.info("end consumer")
