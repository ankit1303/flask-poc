import logging

from flask import current_app
from kombu.entity import Exchange, Queue

from flask_poc.consumers.base_consumer import BaseConsumer

logger = logging.getLogger(__name__)


class DirectConsumer(BaseConsumer):
    def __init__(self, event_type):
        self.exchange_name = str(current_app.config.get('FLASK_POC_VALUE_EXCHANGE'))
        logger.info("Event type is %s", event_type)
        super(DirectConsumer, self).__init__(str(current_app.config.get('RABBITMQ_BROKER_URL')))
        self.exchange = Exchange(self.exchange_name)
        self.queue = Queue(self.get_queue_name(event_type), self.exchange, routing_key=self.get_routing_key(event_type))

    def get_routing_key(self, event_type):
        return str(current_app.config.get('FLASK_POC_ROUTING_KEY')) + str(event_type)

    def get_queue_name(self, event_type):
        return str(current_app.config.get('FLASK_POC_QUEUE')) + str(event_type)
