import functools
from flask_poc.extensions import db


def remove_db_session(func):
    """
    Decorator to explicitly close db session during threaded execution
    """

    @functools.wraps(func)
    def _remove_db_session(*args, **kwargs):
        ret = None
        try:
            ret = func(*args, **kwargs)
        finally:
            db.session.remove()
        return ret

    return _remove_db_session
