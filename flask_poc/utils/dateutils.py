import datetime

import pytz

TIME_ZONE = 'Asia/Kolkata'
timezone = pytz.timezone(TIME_ZONE)


def date_range_inclusive(start, end):
    delta = (end - start).days
    return (start + datetime.timedelta(days=i) for i in range(delta + 1))


def current_date_in_india_timezone():
    current_date = datetime.datetime.now(tz=timezone).date()
    return current_date


def get_all_weekends_from_start_date_to_end_date(start_date, end_date, weekends):
    def is_weekend(d):
        return weekends[d.weekday()] == 1

    return [date
            for date in date_range_inclusive(start_date, end_date)
            if is_weekend(date)]


def ymd_str_to_date(date_ymd_str):
    try:
        return datetime.datetime.strptime(date_ymd_str, '%Y-%m-%d').date()
    except:
        return date_ymd_str


def date_to_ymd_str(d):
    try:
        return datetime.datetime.strftime(d, '%Y-%m-%d')
    except:
        return d
