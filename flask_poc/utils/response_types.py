import flask


def xml_response(xml_string):
    resp = flask.Response(xml_string)
    resp.headers['content_type'] = 'text/xml'
    return resp
