from flask_poc.database import Model, Column, db


class Data(Model):
    __tablename__ = 'value'

    date = Column(db.Date(), nullable=False, primary_key=True)
    value = Column(db.Integer(), nullable=False)

    def __repr__(self):
        return str(self.date)
