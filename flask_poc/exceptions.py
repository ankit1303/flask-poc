class ConfigServiceException(Exception): pass

class InvalidRequestDataError(Exception):
    pass
