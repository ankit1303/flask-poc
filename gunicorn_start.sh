#!/bin/bash

SOCKFILE=/var/run/gunicorn.sock  # we will communicte using this unix socket
echo "Starting $NAME as `whoami`"

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
echo $RUNDIR
test -d $RUNDIR || mkdir -p $RUNDIR

PROJECT_BASE=/usr/src/app
export PYTHONPATH=$PROJECT_BASE:$PYTHONPATH

echo "Applying Migration"
# Django and python 2
python manage.py migrate --noinput

echo 'yes'|python manage.py collectstatic

echo "Starting Gunicorn"
# Django
gunicorn -c deployment_config/gunicorn_config.py wsgi:application

# Flask
#newrelic-admin run-program gunicorn -c deployment_config/gunicorn_config.py manage:app