import os
from psycogreen.gevent import patch_psycopg

bind = '0.0.0.0:8001'

reload = True
workers = 4

worker_class = 'gevent'

worker_connections = 1000
timeout = 30

pidfile = '/var/run/flask_poc_gunicorn.pid'

accesslog = os.environ.get('GUNICORN_ACCESSLOG', '/var/log/flask_poc/gunicorn.log')
errorlog = os.environ.get('GUNICORN_ERRORLOG', '/var/log/flask_poc/gunicorn_error.log')

loglevel = 'debug'

proc_name = 'flask_poc'


def post_fork(server, worker):
    # patch psycopg2 for gevent compatibility
    patch_psycopg()
