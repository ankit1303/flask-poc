###Dockerfile

# FROM directive instructing base image to build upon
FROM python:3.6.3-alpine3.6
# 3.6.3-alpine3.6


ARG req_file=requirements.txt
RUN mkdir -p /usr/src/app /usr/src/static /usr/src/logs /usr/src/app/logs /var/log/its/
RUN touch /usr/src/logs/gunicorn.log /usr/src/logs/gunicorn_error.log
WORKDIR /usr/src/app

COPY requirements /usr/src/app/requirements
COPY requirements.txt /usr/src/app/

RUN apk add --update bash \
    && rm -rf /var/cache/apk/* \
    && apk add --repository http://dl-cdn.alpinelinux.org/alpine/v3.6/main --no-cache py3-psycopg2 \
    && apk add --no-cache --virtual .build-deps \
    build-base postgresql-dev libffi-dev python-dev libxslt-dev libxml2-dev \
    && find /usr/local \
        \( -type d -a -name test -o -name tests \) \
        -o \( -type f -a -name '*.pyc' -o -name '*.pyo' \) \
        -exec rm -rf '{}' + \
    && runDeps="$( \
        scanelf --needed --nobanner --recursive /usr/local \
                | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
                | sort -u \
                | xargs -r apk info --installed \
                | sort -u \
    )" \
    && apk add --virtual .rundeps libxml2 libxslt $runDeps \
    && pip3 install -r $req_file \
    && apk del .build-deps

COPY . /usr/src/app/
COPY scripts /usr/src/scripts

EXPOSE 8000

RUN chmod -R +x /usr/src/scripts

#ENV PYTHONPATH $PYTHONPATH:/usr/src/app/

# EXPOSE port 8000 to allow communication to/from server

