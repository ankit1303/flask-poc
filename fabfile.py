from fabric.api import env, run, sudo, local, put, require, prompt, cd, lcd, settings, task
from fabric.operations import require
from fabric.contrib.files import exists
from collections import OrderedDict

containers = OrderedDict()
containers['web_server'] = 0
containers['cron'] = 0
containers['event_consumer'] = 0

MUMBAI_SERVER = False

consumer_list = [
    'inventory_throttling_service.infrastructure.consumer.AvailabilitySyncEventConsumer',
    'inventory_throttling_service.infrastructure.consumer.HxMasterInventoryConsumer',
    'inventory_throttling_service.infrastructure.consumer.InventoryBroadcastConsumer',
    'inventory_throttling_service.infrastructure.consumer.ThrottlingRuleDirectConsumer',
    'inventory_throttling_service.infrastructure.consumer.ThrottledInventoryEventConsumer',
    'inventory_throttling_service.infrastructure.consumer.MMThrottlingRuleConsumer',
    'inventory_throttling_service.infrastructure.consumer.CatalogServiceConsumer',
]

web_server_name = ''


@task
def push_image(tag_name, version):
    local('docker push %(DOCKER_REGISTRY)s/%(tag_name)s:%(version)s ' % {
        'DOCKER_REGISTRY': env.DOCKER_REGISTRY,
        'tag_name': tag_name, 'version': version
    })


@task
def build(tag_name, version):
    require('env_file', provided_by=(prod, dev, env.run,))
    local('docker build --build-arg req_file=%(req_file)s -t %(docker_registry)s/%(tag_name)s:%(version)s ' \
          '.' % {
              'version': version, 'docker_registry': env.DOCKER_REGISTRY,
              'tag_name': tag_name, 'req_file': env.req_file
          })
    push_image(tag_name, version)


@task
def tag_image(image_id, tag_name, version):
    require('env_file', provided_by=(prod, dev, env.run,))
    local('docker tag %(image_id)s %(DOCKER_REGISTRY)s/%(tag_name)s:%(version)s' % {
        'DOCKER_REGISTRY': env.DOCKER_REGISTRY,
        'image_id': image_id, 'tag_name': tag_name,
        'version': version
    })


@task
def pull_image(tag_name, version):
    sudo('docker pull %(DOCKER_REGISTRY)s/%(tag_name)s:%(version)s' % {
        'DOCKER_REGISTRY': env.DOCKER_REGISTRY,
        'tag_name': tag_name, 'version': version
    })


def create_network():
    local('docker network create -d bridge inventory')


def create_gunicorn_container(tag_name, version, container_no, network=None):
    local('echo creating gunicorn container')
    docker_options = env.copy()
    docker_options.update({'tag_name': tag_name, 'version': version})
    docker_options['name'] = 'inventory_app_server_' + str(container_no)
    global web_server_name
    web_server_name = docker_options['name']
    local('echo starting container %s' % docker_options['name'])
    if network:
        docker_options['network'] = 'inventory'
        sudo('docker run  -v %(log_root)s:%(log_root)s -v %(static_root)s:/usr/src/static ' \
             ' --log-opt max-size=50m  --name %(name)s -p%(server_port)s:8000 --network=%(network)s ' \
             '--env-file=%(env_file)s -itd %(DOCKER_REGISTRY)s/%(tag_name)s:%(version)s ' \
             '%(gunicorn_script_path)s' % docker_options)
    else:
        command = 'docker run  -v %(log_root)s:%(log_root)s -v %(static_root)s:/usr/src/static ' \
                  ' --log-opt max-size=50m ' \
                  '--name %(name)s ' \
                  '-p %(server_port)s:8000 --env-file=%(env_file)s -itd %(DOCKER_REGISTRY)s/%(tag_name)s:%(version)s ' \
                  '%(gunicorn_script_path)s' % docker_options
        sudo(command)


def create_kombu_worker_containers(tag_name, version, container_no, network=None):
    docker_options = env.copy()
    docker_options.update({'tag_name': tag_name, 'version': version})
    for consumer in consumer_list:
        command = 'flask event_consumer --consumer=%s --threaded=%s' % (consumer, False)
        docker_options['command'] = command
        docker_options['name'] = 'inventory_' + command.split()[1] + '_' + consumer.split('.')[-1] + '_' + str(
            container_no)
        docker_options['web_server_name'] = web_server_name
        local('echo starting container %s' % docker_options['name'])
        if network:
            docker_options['network'] = network
            sudo(
                'docker run -v %(log_root)s:%(log_root)s  --log-opt max-size=50m --name %(name)s --env-file=%(env_file)s ' \
                '--network=%(network)s -itd %(DOCKER_REGISTRY)s/%(tag_name)s:%(version)s ' \
                '%(command)s' % docker_options)
        else:
            sudo(
                'docker run -v %(log_root)s:%(log_root)s --log-opt max-size=50m --name %(name)s --env-file=%(env_file)s ' \
                '-itd %(DOCKER_REGISTRY)s/%(tag_name)s:%(version)s ' \
                '%(command)s' % docker_options)


# def create_celery_worker_containers(tag_name, version, container_no, queue_name, no_of_workers, network=None):
#     docker_options = env.copy()
#     docker_options.update({'tag_name': tag_name, 'version': version, 'no': container_no, 'web_server_name': web_server_name,
#                            'queue_name': queue_name, 'no_of_workers': no_of_workers})

#     worker_commands = ('newrelic-admin run-program celery worker -c %(no_of_workers)s -A webapp -l DEBUG -Q %(queue_name)s '
#                        '-f %(log_root)s/celery-worker.log' % docker_options,
#                        )
#     for command in worker_commands:
#         docker_options['name'] = "inventory_" + command.split()[2] + '_' + str(queue_name) + '_' + str(container_no)
#         docker_options['command'] = command
#         local('echo command is %s' % command)
#         local('echo starting container %s' % docker_options['name'])
#         if network:
#             docker_options['network'] = network
#             sudo('docker run -v %(log_root)s:/usr/src/logs --volumes-from %(web_server_name)s --log-opt max-size=50m --name %(name)s --env-file=%(env_file)s ' \
#                  '--network=%(network)s -itd %(DOCKER_REGISTRY)s/%(tag_name)s:%(version)s ' \
#                  '%(command)s' % docker_options)
#         else:
#             sudo('docker run -v %(log_root)s:/usr/src/logs --volumes-from %(web_server_name)s --log-opt max-size=50m --name %(name)s --env-file=%(env_file)s ' \
#                  '-itd %(DOCKER_REGISTRY)s/%(tag_name)s:%(version)s ' \
#                  '%(command)s' % docker_options)


def create_cron_container(tag_name, version, container_no, network=None):
    docker_options = env.copy()
    docker_options.update({'tag_name': tag_name, 'version': version})
    docker_options['name'] = "inventory_" + 'cron_' + str(container_no)
    docker_options['web_server_name'] = web_server_name
    local('echo starting container %s' % docker_options['name'])
    if network:
        docker_options['network'] = network
        sudo(
            'docker run -v %(log_root)s:%(log_root)s -v /var/run/docker.sock:/var/run/docker.sock --log-opt max-size=50m --name %(name)s '
            '--env-file=%(env_file)s  -e DOCKER_IMAGE=%(DOCKER_REGISTRY)s/%(tag_name)s:%(version)s --network=%(network)s '
            '-itd %(DOCKER_REGISTRY)s/%(tag_name)s:%(version)s newrelic-admin run-program /bin/sh -c "crontab cron_jobs; crond -f"' % docker_options)
    else:
        sudo(
            'docker run -v %(log_root)s:%(log_root)s -v /var/run/docker.sock:/var/run/docker.sock --log-opt max-size=50m --name %(name)s '
            '--env-file=%(env_file)s  -e DOCKER_IMAGE=%(DOCKER_REGISTRY)s/%(tag_name)s:%(version)s '
            '-itd %(DOCKER_REGISTRY)s/%(tag_name)s:%(version)s newrelic-admin run-program /bin/sh -c "crontab cron_jobs; crond -f"' % docker_options)


container_creation_methods = {
    'web_server': create_gunicorn_container,
    # 'celery_queues': create_celery_worker_containers,
    'cron': create_cron_container,
    'event_consumer': create_kombu_worker_containers,
}


@task
def prod():
    require('hosts')
    env.run = run
    env.req_file = 'requirements/prod.txt'
    env.env_file = '/etc/its/docker_env/prod.env'
    if not exists('/etc/its/docker_env'):
        sudo('mkdir -p /etc/its/docker_env')
    if not exists('/var/log/its'):
        sudo('mkdir -p /var/log/its')
    if not exists('/home/inventory/static'):
        sudo('mkdir -p /home/inventory/static')
    env.log_root = '/var/log/its'


@task
def dev():
    env.run = run
    if not env.hosts:
        env.hosts.extend(['172.40.20.210'])
    env.req_file = 'requirements/dev.txt'
    env.env_file = '/etc/its/docker_env/dev.env'
    if not exists('/etc/its/'):
        sudo('mkdir -p /etc/its/')
    put('docker_env', '/etc/its/', use_sudo=True)
    if not exists('/var/log/its'):
        sudo('mkdir -p /var/log/its')
    if not exists('/home/inventory/static'):
        sudo('mkdir -p /home/inventory/static')
    env.log_root = '/var/log/its'
    env.DOCKER_REGISTRY = 'docker-hub.treebo.com:5000'


@task
def staging():
    env.run = run
    if not env.hosts:
        env.hosts.extend(['172.40.10.104'])
    env.req_file = 'requirements/staging.txt'
    env.env_file = '/etc/its/docker_env/staging.env'
    if not exists('/etc/its/'):
        sudo('mkdir -p /etc/its/')
    put('docker_env', '/etc/its/', use_sudo=True)
    if not exists('/var/log/its'):
        sudo('mkdir -p /var/log/its')
    if not exists('/home/inventory/static'):
        sudo('mkdir -p /home/inventory/static')
    env.log_root = '/var/log/its'
    env.DOCKER_REGISTRY = 'docker-hub.treebo.com:5000'


@task
def localhost():
    env.run = local
    env.cd = lcd
    env.req_file = 'requirements/dev.txt'
    env.env_file = '/tmp/inventory/docker_env/local.env'
    env.log_root = '/Users/ansilkareem/workspace/logs'
    env.user = 'ansilkareem'
    env.DOCKER_REGISTRY = 'docker-hub.treebo.com:5000'
    env.static_root = '/Users/ansilkareem/workspace/logs'
    # env.gunicorn_log = '/Users/ansilkareem/workspace/logs'

@task
def container_config(web_server, cron, event_consumer, no_of_event_consumer):
    if web_server == 'true':
        containers['web_server'] = 1
    if cron == 'true':
        containers['cron'] = 1
    if event_consumer == 'true':
        containers['event_consumer'] = int(no_of_event_consumer)

@task
def deploy(tag_name, version, user=None):
    if user:
        env.user = user
    require('env_file', provided_by=(prod, dev, env.run,))
    build(tag_name, version)
    # put('docker_env', '/etc/its/', use_sudo=True)
    pull_image(tag_name, version)
    remove_containers()
    create_containers(tag_name, version)


@task
def rollback_to_version(tag_name, version, user=None):
    if user:
        env.user = user
    require('env_file', provided_by=(prod, dev, env.run,))
    pull_image(tag_name, version)
    remove_containers()
    create_containers(tag_name, version)


@task
def remove_containers():
    require('env_file', provided_by=(prod, dev, env.run,))

    local('echo Getting container names')
    names = []
    try:
        a = sudo("docker inspect --format='{{.Name}}' $(sudo docker ps -aq --no-trunc)|grep inventory")
        names = [i.strip('/') for i in a.split()]
    except:
        local('echo No containers found')
    if names:

        containers = ' '.join(names)
        local('echo Stoping Containers')
        try:
            sudo('docker stop %s' % containers)
        except Exception as ex:
            local('echo %s' % ex)

        local('echo Removing Containers')
        try:
            sudo('docker rm %s' % containers)
        except Exception as ex:
            local('echo %s' % ex)
    else:
        local('echo no containers found')


@task
def create_containers(tag_name, version):
    local('echo starting containers')
    for key in containers:
        for i in xrange(1, containers[key] + 1):
            container_creation_methods[key](tag_name, version, i)
